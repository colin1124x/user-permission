# colin/user-permission
使用者權限管理
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/colin1124x/user-permission/badges/quality-score.png?b=master&s=5c05d26c0ac37974a291baf0f7af6228ecfb0ecd)](https://scrutinizer-ci.com/b/colin1124x/user-permission/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/b/colin1124x/user-permission/badges/coverage.png?b=master&s=cccf9b7a4de6791b47342e0b433ce48e81cefad7)](https://scrutinizer-ci.com/b/colin1124x/user-permission/?branch=master)
[![Build Status](https://scrutinizer-ci.com/b/colin1124x/user-permission/badges/build.png?b=master&s=8dde27a1e71161a5ddf0ee5c87085c888e968bda)](https://scrutinizer-ci.com/b/colin1124x/user-permission/build-status/master)

## 前置作業
 - 替 laravel 的 Auth 系統資料表額外添加一個欄位 `secret` 資料型態為 `charset(16)`

## otp (one time password) 驗證方式
 - 在手機安裝 [Google Authenticator]
 - 啟動 app 並輸入16碼密鑰
 - 登入時附上 otp={app上的6位數字} ~~同時祈禱沒有 bug~~

## 資料結構

```php
// @permission_data
array(
    'your/system/path' => array('get', 'put'),
);
```

## example

```php
$permission = new Colin\UserPermission\UserPermission($user_id);

// 設定權限
$permission->add($others); // @permission_data

// 設定單一 path
$permission->set('path/*', array('GET'));

// 設定全部權限
$permission->setAll($rules); // @permission_data

// 移除權限
$permission->remove('path/*');

// 儲存設定
// 只要知道就好, service provider 已經註冊好儲存方法
$permission->setSaveHandler($closure);
$permission->save(); // return boolean

```

## controller api

|method name |url params      |arguments      |return  |
|------------|----------------|---------------|--------|
|lists       |user_id         |-              |array   |
|add         |user_id         |array          |array   |
|set         |user_id         |path, methods  |array   |
|remove      |user_id         |path           |array   |


[Google Authenticator]:https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2
