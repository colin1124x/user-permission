<?php

Route::filter('permissions', function($route, $request){
    /** @var \Colin\UserPermission\UserPermission $permission */
    $permission = App::make('user.permission', array(
        // todo 從資料庫取得
    ));
    if ( ! $permission->check($uri = $route->uri(), $method = $request->method())) {
        return Response::make("抱歉,您沒有使用[{$method} {$uri}]的權限", 401);
    }
});
