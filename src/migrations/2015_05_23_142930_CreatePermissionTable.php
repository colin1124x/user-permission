<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionTable extends Migration {

    public function up()
    {
        Schema::create('permissions', function(Blueprint $table)
        {
            $table->integer('user_id')->unsigned()->primary();
            $table->text('path_rules');
            $table->timestamps();

            $table->index('user_id');
        });
    }

    public function down()
    {
        Schema::drop('permissions');
    }

}
