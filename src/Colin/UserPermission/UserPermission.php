<?php namespace Colin\UserPermission;

class UserPermission
{
    private $config = array();
    private $save_handler;

    public function setSaveHandler(\Closure $handler)
    {
        $this->save_handler = $handler;

        return $this;
    }

    public function save()
    {
        if ( ! $this->save_handler) return false;
        return call_user_func($this->save_handler, $this->config);
    }

    protected function resolveRule($rule)
    {
        return '#^'.str_replace('*', '.+', $rule).'$#';
    }

    protected function filterMethod($methods)
    {
        return array_map(function($m){return strtoupper($m);}, (array) $methods);
    }

    public function add(array $config)
    {
        foreach ($config as $path => $methods) {
            if (empty($methods)) continue;

            $methods = $this->filterMethod($methods);
            if (isset($this->config[$path])) {
                $this->config[$path] = array_unique(array_merge($this->config[$path], $methods));
            } else {
                $this->set($path, $methods);
            }
        }

        return $this;
    }

    public function set($path, array $methods)
    {
        $methods = $this->filterMethod($methods);

        if (empty($methods)) return $this->remove($path);

        $this->config[$path] = $methods;
        return $this;
    }

    public function setAll(array $config)
    {
        $this->config = array();
        $this->add($config);
        return $this;
    }

    public function remove($path)
    {
        unset($this->config[$path]);
        return $this;
    }

    public function check($path, $method)
    {
        $method = strtoupper($method);
        foreach ($this->config as $rule => $methods) {
            if (preg_match($this->resolveRule($rule), $path) && in_array($method, (array) $methods)) {
                return true;
            }
        }
        return false;
    }

    public function lists()
    {
        return $this->config;
    }
}
