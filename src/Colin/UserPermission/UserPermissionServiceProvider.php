<?php namespace Colin\UserPermission;

use Illuminate\Support\ServiceProvider;

class UserPermissionServiceProvider extends ServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->package('colin/user-permission');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('user.permission', function($app, $user_id){

            $user_id = (int) $user_id;
            if ( ! ($m = \Permission::find($user_id))) {
                $m = new \Permission;
                $m->{'user_id'} = $user_id;
                $m->{'path_rules'} = json_encode(new \stdClass());
            }

            $permission = new UserPermission;
            $permission->setAll(json_decode($m->{'path_rules'}, true));
            $user_id and $permission->setSaveHandler(function(array $config) use($m, $user_id) {
                $m->{'path_rules'} = json_encode($config);
                return $m->save();
            });

            return $permission;
        });

        $this->app->bindShared('user.otp', function($app){
            return new Otp();
        });

        include __DIR__.'/../../routes.php';
        include __DIR__.'/../../filters.php';
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array('user.permission', 'user.otp');
    }

}
