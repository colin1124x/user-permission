<?php namespace Colin\UserPermission;

use Illuminate\Support\Facades\Config;
use Otp\GoogleAuthenticator;
use Otp\Otp as OtpOtp;
use Base32\Base32;

class Otp
{
    private $otp;
    public function __construct()
    {
        $this->otp = new OtpOtp();
    }

    public function createSecret()
    {
        return GoogleAuthenticator::generateRandom();
    }

    public function getQrCodeUrl($secret, $label)
    {
        return GoogleAuthenticator::getQrCodeUrl('totp', $label, $secret);
    }

    public function checkSecretByKey($secret, $key, $timedrift = 1)
    {
        if (false === Config::get('user-permission::use-otp')) return true;
        return $this->otp->checkTotp(Base32::decode($secret), $key, $timedrift);
    }

    public function createKeyBySecret($secret, $timecounter = null)
    {
        return $this->otp->totp($secret, $timecounter);
    }
}
