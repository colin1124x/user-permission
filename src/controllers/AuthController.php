<?php

class AuthController extends \Controller
{
    /**
     * 執行登入
     * Request Payload
     * - email (root 登入時只能帶入 "root" 字串)
     * - password
     *
     * @return array|Illuminate\Http\Response 登入執行結果
     */
    public function login()
    {
        $valid = Auth::attempt(array(
            'email' => Input::get('email'),
            'password' => Input::get('password'),
        ));

        // 如果有指定 secret 欄位,代表需作 OTP 檢查
        if (($user = Auth::user()) && ! empty($user['secret'])) {
            /** @var \Colin\UserPermission\Otp $otp */
            $otp = App::make('user.otp');
            if ( ! $otp->checkSecretByKey($user['secret'], Input::get('otp'))) {
                Auth::logout();
                return Response::make("OTP ERROR", 401);
            }
        }

        return $valid ?
            array('success' => true) :
            array('error' => '帳密錯誤');
    }

    /**
     * 執行登出程序
     *
     * @return array
     */
    public function logout()
    {
        Auth::logout();
        return Auth::check() ?
            array('error' => '登出資料清除失敗') :
            array('success' => true);
    }

    /**
     * 僅提示作用,實際系統不應該看到
     * @param array $parameters
     * @return string
     */
    public function missingMethod($parameters = array())
    {
        return 'missing';
    }
}
