<?php

class UserPermissionController extends Controller
{
    /**
     * @param $user
     * @return Colin\UserPermission\UserPermission
     */
    private function buildPermission($user)
    {
        return App::make('user.permission', (int)$user);
    }

    public function lists($user)
    {
        return $this->buildPermission($user)->lists();
    }

    public function add($user)
    {
        $p = $this->buildPermission($user)->add(Input::get());
        $p->save();
        return $p->lists();
    }

    public function set($user)
    {
        $usage = "Usage: path=your/path, methods[]=get, methods[]=put...";
        if ( ! ($path = Input::get('path'))) return Response::make($usage, 500);
        $methods = (array)Input::get('methods');
        $p = $this->buildPermission($user)->set(
            $path,
            $methods
        );
        $p->save();
        return $p->lists();
    }

    public function remove($user)
    {
        $usage = "Usage: path=your/path";
        if ( ! ($path = Input::get('path'))) return Response::make($usage, 500);
        $p = $this->buildPermission($user)->remove($path);
        $p->save();
        return $p->lists();
    }
}
