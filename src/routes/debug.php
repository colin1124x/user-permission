<?php

class FakeRequest {
    private $method;
    public function __construct($m){$this->method = strtoupper($m);}
    public function method(){return $this->method;}
}

Route::group(array('prefix' => 'debug'), function(){

    // 檢查指定系統, path = debug/check/permissions
    Route::get('check/permissions', function(){

        if ( ! Request::input('path') || ! Request::input('method')) {
            return "
                Usage: GET path=your/path, method=your/method
            ";
        }
        $events = Event::until('router.filter: permissions', array(
            // 模擬特定 route
            new \Illuminate\Routing\Route(
                Request::input('method'),
                Request::input('path'),
                array()
            ),
            // 模擬特定 request
            new FakeRequest(Request::input('method'))
        ));

        return $events ?: "你有權限進入".Route::current()->uri();
    });
});
