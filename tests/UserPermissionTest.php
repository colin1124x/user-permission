<?php

class UserPermissionTest extends PHPUnit_Framework_TestCase
{
    private function buildInstance()
    {
        return new Colin\UserPermission\UserPermission;
    }

    public function testList()
    {
        $config = array(
            'a/b' => array('get'),
        );
        $o = $this->buildInstance()->setAll($config);

        $this->assertEquals(array('a/b' => array('GET')), $o->lists());
    }

    public function testAction()
    {
        $config = array(
            'a/b' => array('get'),
        );
        $o = $this->buildInstance()->setAll($config);
        $o->add(array(
            'a/b' => array('Put'),
            'c/d' => array('post'),
        ));

        $this->assertTrue($o->check('a/b', 'GeT'));
        $this->assertTrue($o->check('a/b', 'puT'));
        $this->assertTrue($o->check('c/d', 'pOsT'));

        $o->remove('a/b');

        $this->assertFalse($o->check('a/b', 'GeT'));

        $o->add(array(
            'xyz' => array('get'),
            'xyz/*' => array('post'),
        ));

        $this->assertTrue($o->check('xyz', 'get'));
        $this->assertFalse($o->check('xyz/1', 'get'));
        $this->assertTrue($o->check('xyz/1', 'post'));

        $o->set('xyz/*', array());

        $this->assertFalse($o->check('xyz/*', 'post'));
    }

    public function testSave()
    {
        $tester = $this;
        $config = array(
            'a/b' => array('get'),
        );
        $o = $this->buildInstance()->setAll($config);

        $this->assertFalse($o->save());

        $has_called_save_handler = false;
        $o->add($addition = array('c/d' => array('get')));
        $config += $addition;
        $o->setSaveHandler(function() use($tester, &$has_called_save_handler, $config) {
            $has_called_save_handler = true;
            $tester->assertEquals(array(
                'a/b' => array('GET'),
                'c/d' => array('GET'),
            ), func_get_arg(0));
        });
        $o->save();
        $this->assertTrue($has_called_save_handler);
        $this->assertTrue($o->check('c/d', 'GET'));
    }
}
